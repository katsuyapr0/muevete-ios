//
//  Vibration.h
//  Muevete
//
//  Created by Andres Abril on 5/09/13.
//  Copyright (c) 2013 iAmStudio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FileSaver.h"
#import <AudioToolbox/AudioToolbox.h>
@interface Vibration : NSObject
+(void)vibrate;
@end
