//
//  CustomCell.h
//  Muevete
//
//  Created by Andres Abril on 4/09/13.
//  Copyright (c) 2013 iAmStudio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
@interface CustomCell : UITableViewCell{
}
@property(nonatomic,retain) UILabel *timeLabel;
@property(nonatomic,retain) UILabel *dateLabel;
@property(nonatomic,retain) UILabel *metersLabel;

@end
